Toolforge BNC container
=======================

[Build Service][] project creating a container running [ZNC][] which can be
used to bridge a tool to IRC networks.

Use with your tool
------------------
```
$ ssh dev.toolforge.org
$ become $TOOL
$ toolforge envvars create BNC_USER
$ toolforge envvars create BNC_NICK
$ toolforge envvars create BNC_REALNAME
$ toolforge envvars create BNC_PASSWORD
$ toolforge jobs run \
  --image tool-containers/bnc:latest \
  --command bouncer \
  --continuous \
  --emails none \
  --port 6667 \
  bnc
```

Be sure to check `kubectl describe quota` to see if your tool has available
quota for a new Service. If it does not, see [Toolforge (Quota-requests)][]
for information on how to request additional quota.

The container also includes [irssi][] with configuration to connect to the
bouncer:
```
$ ssh dev.toolforge.org
$ become $TOOL
$ webservice buildservice shell \
  --buildservice-image tool-containers/bnc:latest \
  --mount none
I have no name!@shell-1717795447:~$ client
# irssi starts and connects to the ZNC service
# `/win list` to see all current windows
# `/win 2` to switch to the window connected to ZNC's *status command channel
# `/join #channelname` to join a libra.chat channel
# `/quit` to close irssi
```

Publish a new container
-----------------------
```
$ ssh dev.toolforge.org
$ become wikibugs
$ toolforge envvars create IRC_PASSWORD
$ toolforge build start --image-name bnc \
  https://gitlab.wikimedia.org/toolforge-repos/containers-bnc
```

License
-------
Licensed under the [GPL-3.0-or-later][] license. See [COPYING][] for the full
license.

This project was friendly forked from [wikibugs2-znc][].

[Build Service]: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service
[ZNC]: https://wiki.znc.in/ZNC
[Toolforge (Quota-requests)]: https://phabricator.wikimedia.org/project/view/4834/
[irssi]: https://irssi.org/
[GPL-3.0-or-later]: https://www.gnu.org/licenses/gpl-3.0.html
[COPYING]: COPYING
[wikibugs2-znc]: https://gitlab.wikimedia.org/toolforge-repos/wikibugs2-znc
