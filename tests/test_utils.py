# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Toolforge BNC container.
#
# Toolforge BNC container is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Toolforge BNC container is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Toolforge BNC container. If not, see <http://www.gnu.org/licenses/>.
import containers.bnc.utils


def test_extract_password_hash():
    """Do the regex patterns work?"""
    expect_hash = (
        "b3023b56dc61369f396f5af047cc5ef2f17c37b47261ddc2560a3ae53b9d2fb1"
    )
    expect_salt = "uVIi!O76-*bfP,afa(YA"
    fixture = f"""[ ** ] Type your new password.
[ ?? ] Enter password:
[ ?? ] Confirm password:
[ ** ] Kill ZNC process, if it's running.
[ ** ] Then replace password in the <User> section of your config with this:
<Pass password>
        Method = sha256
        Hash = {expect_hash}
        Salt = {expect_salt}
</Pass>
[ ** ] After that start ZNC again, and you should be able to login with the new password."""

    got_hash, got_salt = containers.bnc.utils.extract_password_hash(fixture)
    assert expect_hash == got_hash
    assert expect_salt == got_salt
