# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Toolforge BNC container.
#
# Toolforge BNC container is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Toolforge BNC container is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Toolforge BNC container. If not, see <http://www.gnu.org/licenses/>.
import pathlib

import environ

env = environ.Env()
env.smart_cast = False
environ.Env.read_env(env_file=pathlib.Path.cwd() / ".env")

# == BNC settings ==
BNC_USER = env.str("BNC_USER")
BNC_NICK = env.str("BNC_NICK")
BNC_REALNAME = env.str("BNC_REALNAME")
BNC_NETWORK = env.str("BNC_NETWORK", default="libera")
BNC_NETWORK_SERVER = env.str(
    "BNC_NETWORK_SERVER",
    default="irc.ipv4.libera.chat +6697",
)
BNC_SERVICE_NAME = env.str("BNC_SERVICE_NAME", default="bnc")
BNC_PASSWORD = env.str("BNC_PASSWORD")
