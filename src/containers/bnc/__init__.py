# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Toolforge BNC container.
#
# Toolforge BNC container is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Toolforge BNC container is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Toolforge BNC container. If not, see <http://www.gnu.org/licenses/>.
from . import cli
from .version import __version__  # noqa: F401 imported but unused

if __name__ == "__main__":  # pragma: nocover
    cli.main()
