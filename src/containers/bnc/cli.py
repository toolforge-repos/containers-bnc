# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Toolforge BNC container.
#
# Toolforge BNC container is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Toolforge BNC container is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Toolforge BNC container. If not, see <http://www.gnu.org/licenses/>.
import logging
import os
import pathlib
import sys

import click
import coloredlogs

from . import settings
from . import utils
from .version import __version__

logger = logging.getLogger(__name__)


@click.group()
@click.version_option(version=__version__)
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase debug logging verbosity",
)
@click.pass_context
def main(ctx, verbose):
    """Setup and run an IRC bouncer."""

    coloredlogs.install(
        level=max(logging.DEBUG, logging.WARNING - (10 * verbose)),
        fmt="%(asctime)s %(name)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%SZ",
        level_styles=coloredlogs.DEFAULT_LEVEL_STYLES
        | {
            "debug": {},
            "info": {"color": "green"},
        },
        field_styles=coloredlogs.DEFAULT_FIELD_STYLES
        | {
            "asctime": {"color": "yellow"},
        },
    )
    logging.captureWarnings(True)

    gen_hash, gen_salt = utils.generate_password_hash(settings.BNC_PASSWORD)
    ctx.obj = {
        "BNC_PASSWORD": settings.BNC_PASSWORD,
        "BNC_NETWORK": settings.BNC_NETWORK,
        "BNC_NETWORK_SERVER": settings.BNC_NETWORK_SERVER,
        "BNC_NICK": settings.BNC_NICK,
        "BNC_PASSWORD_HASH": gen_hash,
        "BNC_PASSWORD_SALT": gen_salt,
        "BNC_REALNAME": settings.BNC_REALNAME,
        "BNC_SERVICE_NAME": settings.BNC_SERVICE_NAME,
        "BNC_USER": settings.BNC_USER,
    }


@main.command()
@click.pass_context
def znc(ctx):
    """Run ZNC"""
    logger.info("Generating ZNC config")
    # XXX: should this be configurable?
    znc_config_dir = pathlib.Path.cwd() / ".znc"

    utils.generate_config(
        znc_config_dir / "configs" / "znc.conf",
        "znc.conf",
        ctx.obj,
    )
    utils.generate_config(
        znc_config_dir
        / "users"
        / settings.BNC_USER
        / "networks"
        / settings.BNC_NETWORK
        / "moddata"
        / "sasl"
        / ".registry",
        "sasl.conf",
        ctx.obj,
    )

    logger.info("Starting ZNC")
    sys.stdout.flush()
    sys.stderr.flush()
    os.execlp(
        "znc",
        "znc",
        "--foreground",
        "--debug",
        "--datadir",
        znc_config_dir.resolve(strict=True),
    )


@main.command()
@click.pass_context
def irssi(ctx):
    """Run irssi"""
    logger.info("Generating irssi config")
    irssi_config_dir = pathlib.Path.cwd() / ".irssi"
    utils.generate_config(
        irssi_config_dir / "config",
        "irssi.conf",
        ctx.obj,
    )

    logger.info("Starting irssi")
    sys.stdout.flush()
    sys.stderr.flush()
    os.execlp(
        "irssi",
        "irssi",
        "--home",
        irssi_config_dir.resolve(strict=True),
    )


if __name__ == "__main__":  # pragma: nocover
    main()
