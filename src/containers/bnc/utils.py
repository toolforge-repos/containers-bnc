# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Toolforge BNC container.
#
# Toolforge BNC container is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Toolforge BNC container is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Toolforge BNC container. If not, see <http://www.gnu.org/licenses/>.
import functools
import pathlib
import re
import subprocess

import jinja2

TEMPLATE_DIR = pathlib.Path(__file__).parent / "templates"

RE_GENERATED_HASH = re.compile(r"\bHash = (?P<hash>[0-9a-f]+)")
RE_GENERATED_SALT = re.compile(r"\bSalt = (?P<salt>\S+)")


@functools.cache
def template_environment():
    """Get a jinja2 environment."""
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(TEMPLATE_DIR),
        trim_blocks=True,
        lstrip_blocks=True,
    )


def expand_template(name, context):
    """Expand a jinja2 template."""
    env = template_environment()
    tmpl = env.get_template(name)
    data = tmpl.render(**context)
    if data is None:
        raise RuntimeError(f"Generated empty {name} file")
    return data


def generate_config(config_file_path, template, context):
    """Generate a configuration file."""
    config_dir = config_file_path.parent
    config_dir.mkdir(mode=0o770, parents=True, exist_ok=True)
    config_file_path.touch(mode=0o660, exist_ok=True)
    config_file_path.write_text(expand_template(template, context))


def generate_password_hash(password):
    """Generate a ZNC hash and salt pair for the given password."""
    cmd = subprocess.Popen(
        ["znc", "--makepass"],
        stdin=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        text=True,
    )
    out = cmd.communicate(input=f"{password}\n{password}\n")[0]
    return extract_password_hash(out)


def extract_password_hash(output):
    """Extract ZNC hash and salt pair from command output."""
    out_hash = RE_GENERATED_HASH.search(output).group("hash")
    out_salt = RE_GENERATED_SALT.search(output).group("salt")
    return out_hash, out_salt
